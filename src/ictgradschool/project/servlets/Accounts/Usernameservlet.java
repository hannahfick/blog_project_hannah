package ictgradschool.project.servlets.Accounts;

import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Usernameservlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDAO dao = new UserDAO(getServletContext());
        List<String> user_idList = dao.getuser_ids();
        //start making our json structure
        Map user_idsMap = new HashMap();
        user_idsMap.put("user_ids", user_idList);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(new JSONObject().toJSONString(user_idsMap));

    }
}
