package ictgradschool.project.servlets.Articles;

import ictgradschool.project.servlets.Accounts.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


@WebServlet(name = "ArticleMaker")
public class ArticleMaker extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //sets basic article info
        String title = request.getParameter("title");
        String content = request.getParameter("content");
        String publish_time = request.getParameter("publish_time");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date javaPT = null;
        try {
            javaPT = format.parse(publish_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.sql.Date sqlPT = new java.sql.Date(javaPT.getTime());


        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        ArticleDAO dao = new ArticleDAO(getServletContext());

        //creates article and returns ID
        int articleID = dao.addArticle(title, user, content, sqlPT);
        //send ID to UserMediaServlet to get media list and go to edit page
        request.setAttribute("articleID", String.valueOf(articleID));
        session.setAttribute("articleID", articleID);
        request.getRequestDispatcher("UserMediaServlet").forward(request, response);
    }

    //used by the login servlet as landing after successful login. line 51. Also called by delete link buttons.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        //if creating new article send to doPost
        if (request.getParameter("newArticle") != null) {
            doPost(request, response);
        }

        //checking if get req is for deleting an article
        if (request.getParameter("delArticleID") != null) {
            int articleID = Integer.parseInt(request.getParameter("delArticleID"));
            String user_id = request.getParameter("user_id");
            if (!(new ArticleDAO(getServletContext()).delArticle(articleID, user_id))) {
                request.setAttribute("denied", "denied");
            }
        }

        if (request.getParameter("getuserarticles") != null) {
            ArticleDAO dao = new ArticleDAO(getServletContext());
            request.setAttribute("articles", dao.getArticleByUserID(request.getParameter("articleUserID")));
            request.getRequestDispatcher("ArticleView.jsp").forward(request, response);
            return;
        }

        //else add articles by logged in user to the request and send to article view page
        request.setAttribute("articles", getArticlesByUser(user));
        request.getRequestDispatcher("ArticleView.jsp").forward(request, response);
    }

    private ArrayList<Article> getArticlesByUser(User user) {
        ArticleDAO dao = new ArticleDAO(getServletContext());
        return dao.getArticles(user);
    }
}
