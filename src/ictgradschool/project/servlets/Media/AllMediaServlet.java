package ictgradschool.project.servlets.Media;

import ictgradschool.project.servlets.Accounts.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "AllMediaServlet")
@MultipartConfig
public class AllMediaServlet extends HttpServlet {
    private File uploadfolder;

    @Override
    public void init() throws ServletException {
        super.init();

        this.uploadfolder = new File("/Photos/MediaUploads");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        File uploadsFolder = new File(request.getServletContext().getRealPath(String.valueOf(uploadfolder)));
        File[] userFolders = uploadsFolder.listFiles();

        List<String> mediaList = new ArrayList<>();
        if (userFolders != null) {
            for (File file : userFolders
            ) {
                //loop user folders and list contents then add to mediaList.
                File userFolder = new File(String.valueOf(file));
                File[] userFiles = userFolder.listFiles();
                if (userFiles != null) {
                    for (int i = 0; i <= userFiles.length - 1; i++) {
                        mediaList.add(".\\Photos\\MediaUploads\\" + file.getName() + "\\" + userFiles[i].getName());
                    }
                }
            }
        }
        request.setAttribute("mediaList", mediaList);
        request.getRequestDispatcher("MediaUploadGallery.jsp").forward(request, response);
    }
}



