package ictgradschool.project.servlets.Media;

import ictgradschool.project.servlets.Accounts.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "MediaUploadServlet")
@MultipartConfig
public class UserMediaServlet extends HttpServlet {

    private File uploadfolder;

    @Override
    public void init() throws ServletException {
        super.init();
        //check upload folder exists, if not create it
        this.uploadfolder = new File(getServletContext().getRealPath("/Photos/MediaUploads"));
        if (!uploadfolder.exists()) {
            uploadfolder.mkdir();
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameter("articleID") != null) {
            request.setAttribute("articleID", request.getParameter("articleID"));
        } else if (request.getAttribute("articleID").toString() != null) {
            request.setAttribute("articleID", request.getAttribute("articleID"));
        }
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String articleID = request.getAttribute("articleID").toString();
        if (articleID == null) {
            articleID = session.getAttribute("articleID").toString();
        }

        File userFolder = new File(String.valueOf(uploadfolder) + "//" + user.getuser_id());
        if (!userFolder.exists()) {
            userFolder.mkdir();
        }

        File photosFolder = new File(String.valueOf(userFolder));
        File[] files = photosFolder.listFiles();


        List<String> mediaList = new ArrayList<>();

        for (File file : files
        ) {
            mediaList.add(".\\Photos\\MediaUploads" + "\\" + user.getuser_id() + "\\" + file.getName());
        }
        request.setAttribute("mediaList", mediaList);

        if (articleID != null) {
            request.setAttribute("articleID", articleID);
            request.getRequestDispatcher("EditArticleServlet").forward(request, response);
        } else {
            request.getRequestDispatcher("ArticleCreate.jsp").forward(request, response);

        }
    }
}



