package ictgradschool.project.servlets.Utils;

import ictgradschool.project.servlets.Articles.Article;
import ictgradschool.project.servlets.Articles.ArticleDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet(name = "HomePageSetup")
public class HomePageSetup extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        ArticleDAO dao3 = new ArticleDAO(getServletContext());
        ArrayList<Article> HParticles = dao3.getHPArticles();
        request.setAttribute("HParticles", HParticles);
        request.getRequestDispatcher("HomePage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}






