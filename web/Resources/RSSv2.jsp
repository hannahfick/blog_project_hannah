<%--
  Created by IntelliJ IDEA.
  User: dsten
  Date: 7/02/2019
  Time: 6:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
              integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
              crossorigin="anonymous">

        <%--button--%>
        <link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
        <%--title--%>
        <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">

        <%--date--%>
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <style>

            .card-header {
                border: 1px solid #ddd;
                border-radius: 4px 4px 0px 0px;
                margin-top: 1px;
                margin-bottom: 0px;
                background-color: #e4e4e4;
                /*background-image: url("zig.jpg");*/
            }

            .titlestyle {

                background-color: white;
                background-image: none;
                border: 1px solid #ddd;
                border-radius: 4px 4px 0px 0px;
                margin-top: 1px;
                margin-bottom: 0px;
                padding-bottom: -1px;

            }

            .story {
                background-color: #ebebeb;
                padding-bottom: 5px;
            }

            .feed-name {
                margin-top: 0px;
                margin-bottom: 0px;
                padding-top: 5px;
                padding-bottom: 8px;
                background-color: white;
                text-align: center;

            }

            .button {
                margin-bottom: -15px;
                background-color: white;
            }

            .card-body {
                border: 1px solid #ddd;
                border-bottom: 0px;
                border-top: 0px;
                margin-top: -2px;
                margin-bottom: -2px;
            }

            .card-footer {
                border: 1px solid #ddd;
                padding-top: 2px;
                padding-bottom: 5px;
                border-radius: 0px 0px 4px 4px;
                margin-bottom: 2px;
                font-size: small;
                /*background-color: white;*/
                background-color: #eeeeee;

            }

            /*add tooltip to links to alert opens in new tab*/
            img:hover {
                box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
            }

            /*asdfsd*/
        </style>
    </head>
    <body>
        <core:set var="feedURL" value="https://www.stuff.co.nz/rss/"/>
        <core:if test="${param.feedURL != null}">
            <core:import var="xmlContent" url="${param.feedURL}"/>
        </core:if>
        <core:if test="${param.feedURL==null}">
            <core:import var="xmlContent" url="${feedURL}"/>
        </core:if>
        <x:parse var="doc" xml="${xmlContent}"/>


        <div class="card-header feed-name" style="background-image: none ;font-family: 'Julius Sans One', sans-serif;">
            <i style="float: left" class="material-icons">rss_feed</i><b><x:out select="$doc/rss/channel/title"/></b>
        </div>
        <div class="titlestyle" style="text-align: center">
            <form>
                <input type="text" name="feedURL" class="form-control" placeholder="paste feed url"/>
                <input class="form-control button btn btn-outline-secondary  btn-sm" type="submit" value="DISPLAY"/>
            </form>
        </div>

        <x:forEach var="story"
                   select="$doc/rss/channel/item" varStatus="status">
        <div class="card-header story">
            <a href="<x:out select="link"/>" target="_blank" rel="noopener"
               style="text-decoration: none;color: black;font-family: 'Titillium Web', sans-serif;"><x:out
                    select="title"/></a>
        </div>
        <core:set var="desc">
            <x:out select="description" escapeXml="false"/>
        </core:set>
        <core:if test="${desc.length()>1}">
        <div class="card-body" style="font-family: 'Titillium Web', sans-serif;">
            <x:out select="description" escapeXml="false"/>
        </div>
        </core:if>
        <div class="card-footer" style="font-family: 'Titillium Web', sans-serif;">
            <x:out select="pubDate"/>
        </div>
        </x:forEach>

