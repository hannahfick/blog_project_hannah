<%--
  Created by IntelliJ IDEA.
  User: dwc1
  Date: 30/01/2019
  Time: 3:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<head>
    <meta charset="utf-8">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <%--icon--%>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <%--font--%>
    <%--title--%>
    <link href="https://fonts.googleapis.com/css?family=Monoton" rel="stylesheet">
    <%--button--%>
    <link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
    <%--Date (font)--%>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">

    <style>
        html {
            font-size: 100%;
        }

    </style>
</head>
<nav class="navbar navbar-expand-xl fixed-top" style="position: sticky;background: white;" >

    <%--LOGO and header--%>

    <div class="navbar-brand" style="padding-left: 8%; padding-right: 1%"><a href="HomePageSetup"
                                                                                  class="navbar-left"><img
            src="Resources/BLOGO.png"></a>
    </div>


    <div class="navbar-nav mr-auto " style="font-family: 'Monoton', cursive; font-size: 48pt"><a href="HomePageSetup"
                                                                                              style="text-decoration: none;color: black">BLOG.LOG.O.G</a><a
            href="AllMediaServlet">
        <button class="btn btn btn-link btn-circle btn-lg" aria-label="Center Align">
            <span class="glyphicon glyphicon-film" aria-hidden="true" STYLE="color: black"></span></button>
    </a></div>
        <%--&lt;%&ndash;toggle&ndash;%&gt;--%>
        <%--<button class="navbar-toggler  btn btn btn-link btn-circle btn-lg" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">--%>
             <%--<span class="glyphicon glyphicon-align-justify" aria-hidden="true"--%>
                   <%--style="color: black"></span></span>--%>
        <%--</button>--%>
    <%--changing part--%>
    <core:choose>
        <%--unloged user--%>

        <core:when test="${user.user_id==null}">
        <div align="right">
                <%--toggle--%>
            <button class="navbar-toggler  btn btn btn-link btn-circle btn-lg" type="button" data-toggle="collapse" data-target="#navbarToggler1" aria-controls="navbarToggler1" aria-expanded="false" aria-label="Toggle navigation">
             <span class="glyphicon glyphicon-align-justify" aria-hidden="true"
                   style="color: black"></span></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarToggler1">

            <form class="form-inline my-2 my-lg-0" action="Login" method="post">

                <div style="padding-right: 10px">
                    <input type="text" id="user_id" placeholder="enter user_id" name="user_id" required align="center"
                           autofocus style="outline: none;height: 31px;">
                </div>

                <div style="padding-right: 10px">
                    <input type="password" id="password" placeholder="enter password" name="password" required
                           align="center"
                           autofocus style="outline: none;height: 31px;">

                </div>
                <div style="padding-right: 10px">
                    <button type="submit" class=" button btn btn-outline-secondary  btn-sm" id="loginButton"
                            name="loginButton" style="font-family: 'Rajdhani', sans-serif;">

                        <span class="glyphicon glyphicon-log-in" aria-hidden="true"
                              style="padding-right: 10px"></span><span>LOGIN</span></button>
                </div>
            </form>


            <div style="padding-right: 10%">

                <form action="AccountCreation" method="get" style="margin-block-end: 0px;">
                    <button type="submit" class="btn btn-outline-secondary  btn-sm" id="createAccountButton"
                            name="createAccountButton" aria-label="Left Align"
                            style="font-family: 'Rajdhani', sans-serif;width: 150px"><span class="glyphicon glyphicon-pencil" aria-hidden="true" style="padding-right: 10px"></span>
                        CREATE ACCOUNT
                    </button>


                </form>

            </div>
            </div>
            </div>
        </core:when>
        <%--loged in--%>

        <core:otherwise>
        <div align="right">




                <%--toggle--%>
                <button class="navbar-toggler  btn btn btn-link btn-circle btn-lg" type="button" data-toggle="collapse" data-target="#navbarToggler2" aria-controls="navbarToggler2" aria-expanded="false" aria-label="Toggle navigation">
             <span class="glyphicon glyphicon-align-justify" aria-hidden="true"
                   style="color: black"></span></span>
                </button>
            <div class="collapse navbar-collapse" id="navbarToggler2">
                <p style="padding-right: 10px;padding-top: 1%;font-family: 'Titillium Web'">@${user.user_id}</p>
            <form action="Login" method="post" style="padding-right: 10px">
                <button class="btn btn-outline-secondary  btn-sm" type="submit" id="logout" name="logout"
                        style="font-family: 'Rajdhani', sans-serif;width: 100px">
                    <span class="glyphicon glyphicon-off" aria-hidden="true" style="padding-right: 10px"></span>LOG OUT
                </button>
            </form>
            <%--</div>--%>

            <div style="padding-right: 10px;padding-bottom: 15px">
                <a href="AccountCreation" style="font-family: 'Rajdhani', sans-serif;">
                    <button class="btn btn-outline-secondary  btn-sm" style="width: 130px">
                        <span class="glyphicon glyphicon-user" aria-hidden="true" style="padding-right: 10px"></span>MY

                                                                                                                     ACCOUNT

                    </button>
                </a>

            </div>
            <div style="padding-right: 50px;padding-bottom: 15px"><a class="btn btn-outline-secondary  btn-sm"
                                                                     href="ArticleMaker"
                                                                     style="font-family: 'Rajdhani', sans-serif;width: 130px">
                <span class="glyphicon glyphicon-list-alt" aria-hidden="true" style="padding-right: 10px"></span>MY

                                                                                                                      ARTICLES</a>
            </div>



            <form class="form-inline" action="ADSearch" method="post" style="padding-right: 100px">
                <input name="ADsearchString" type="search" placeholder="Search Title/Author" aria-label="Search"
                       style="outline: none;height: 25px;">
                <button class="btn btn-outline-secondary  btn-sm" type="submit" id="searchButton" name="searchButton"
                        style="font-family: 'Rajdhani', sans-serif;"
                ><span class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 10px"></span>SEARCH
                </button>
            </form>
            </div>
        </div>
        </core:otherwise>
    </core:choose>


    <%--</nav>--%>
</nav>
<%--</nav>--%>