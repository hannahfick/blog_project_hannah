<%--
  Created by IntelliJ IDEA.
  User: dwc1
  Date: 25/01/2019
  Time: 3:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
              integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
              crossorigin="anonymous">

        <%--Google Reply Icons--%>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <%--Bootstrap Icons--%>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
        <%--Title (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
        <%--Date (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>

        <title>Article view</title>
    </head>

    <body>
        <%@include file="Resources/nav.jsp" %>
        <br><br><br><br>

        <div class="container">
            <%--Display article--%>
            <h2 style="font-family: 'Julius Sans One', sans-serif;"><b>${article.title}</b></h2>
            <br>
            <%--Link to all articles from this author --%>
            <a href="ArticleMaker?getuserarticles=true&articleUserID=${article.user_id}" target="_blank"
               class="text-dark">
                <img style="float: right" src="${article.userPicURL}" height="45" width="45">
                <h6 style="margin-right: 50px;text-align: right; font-family: 'Titillium Web', sans-serif;">${article.user_id}</h6>
            </a>
            <p id="gg"
               style="margin-right: 50px;text-align: right; font-family: 'Titillium Web', sans-serif;">${article.publish_time}</p>
            ${article.content}
            <br><br>

            <p>${article.votes} VOTES</p>

            <a href="VoteServlet?articleID=${article.articleID}&voteDirection=up&publish_time=${article.publish_time}"
               class="btn btn-outline-secondary  btn-md">
                <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"> </span>
            </a>
            <a href="VoteServlet?articleID=${article.articleID}&voteDirection=down&publish_time=${article.publish_time}"
               class="btn btn-outline-secondary  btn-md">
                <span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span>
            </a>

            <%--Test to see if user is owner of article to authorise deletion--%>
            <core:if test="${user!=null && (user.user_id == article.user_id)}">
                <a href="ArticleMaker?delArticleID=${article.articleID}&user_id=${user.user_id}"
                   class="btn btn-outline-secondary  btn-md">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </a>
            </core:if>
            <hr>
            <hr size="3px" color="black">
        </div>

        <%--Display Comments--%>
        <div class="container">
            <core:forEach items="${comment}" var="comment">

                <%--Comment (determining if it is an original comment or a reply)--%>
                <div class="card"  <core:out value="${comment.comment_to eq 0 ? '': 'style=margin-left:100px;'}"/>
                     id="<core:out value="${'parent'+=comment.commentID}"/>">

                        <%--Adds user pic to comment--%>
                    <div class="card-body" style="border: none">
                        <div class="row justify-content-between">
                            <div>
                                <img style="margin-left: 15px;margin-bottom: 15px;float: left" height="45" width="45"
                                     src="${comment.userpic_url}">
                                <p style="margin-left: 70px">@${comment.comment_BY}: ${comment.comment_time}</p><p><a class="text-dark" href="#parent${comment.comment_to}"><core:out value="${comment.comment_to eq 0 ? '':  '-->'+=comment.reply_to}"/></a></p></div>

                                <%--Tests whether user is authorised to delete comment--%>
                            <core:if
                                    test="${user!=null && (user.user_id == comment.comment_BY || user.user_id==article.user_id)}">
                                <form action="CommentMaker" method="get" style="padding-right: 30px">
                                    <input type="hidden" value="${article.articleID}" name="articleID">
                                    <input type="hidden" value="${user.user_id}" name="user_id">
                                    <button class="btn btn-outline-secondary btn-sm" type="submit" id="delComment"
                                            name="delCommentID" value="${comment.commentID}">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </form>
                            </core:if>
                        </div>

                        <p>${comment.comment}</p>
                        <hr size="2px" color="light-grey">

                            <%--Allows users to respond to a comment reply--%>
                        <core:if
                                test="${user!=null}">
                            <form action="CommentMaker" method="post">
                                <textarea cols="100" id="replycomment" placeholder="reply to comment"
                                          name="comment">
                                </textarea>
                                <input type="hidden" id="replyconnectToArticle" name="articleID"
                                       value="${article.articleID}">
                                <input type="hidden" id="replycomment_BY" name="comment_BY" value="${user.user_id}">
                                <button class="btn btn-outline-secondary  btn-sm" type="submit" id="replycomment_to"
                                        name="comment_to"
                                        value="${comment.commentID}"><i class="material-icons">reply</i>
                                </button>
                            </form>
                        </core:if>
                    </div>
                </div>
                <span id="<core:out value="${'reply'+=comment.commentID}"/>"></span>
            </core:forEach>

            <%--Append reply comments to their parent--%>
            <core:forEach items="${comment}" var="comment">
                <core:if test="${comment.comment_to != 0}">
                    <script>
                        $("#${'parent'+=comment.commentID}").appendTo($("#${'reply'+=comment.comment_to}"));
                    </script>
                </core:if>
            </core:forEach>
            <br><br>
            <hr>
            <br>
            <%--Tests to see if user is logged in to authorise ability to comment--%>
            <core:if test="${user!=null}">
                <form action="CommentMaker" method="post" style="margin-outside: 100px">
                    <textarea id="comment" placeholder="Enter Comment" name="comment" cols="150" rows="5"></textarea>
                    <input type="hidden" id="connectToArticle" name="articleID" value="${article.articleID}">
                    <input type="hidden" id="comment_BY" name="comment_BY" value="${user.user_id}">
                    <input type="hidden" id="comment_to" name="comment_to" value="0">
                    <button class="btn btn-outline-secondary  btn-sm" type="submit">
                        <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
                    </button>
                </form>
            </core:if>
            <%--Prompt user to log in --%>
            <core:if test="${user==null}">
                <p>Login to comment</p>
            </core:if>
        </div>
    </body>
</html>
