CREATE TABLE group_project_comment
(
    comment_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    comment longtext,
    made_by varchar(80) NOT NULL,
    comment_time timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    comment_to int,
    articel_id int NOT NULL,
    CONSTRAINT group_project_comment_ibfk_1 FOREIGN KEY (made_by) REFERENCES group_project_user_db (user_id) ON DELETE CASCADE,
    CONSTRAINT group_project_comment_ibfk_2 FOREIGN KEY (comment_to) REFERENCES group_project_comment (comment_id) ON DELETE CASCADE,
    CONSTRAINT group_project_comment_ibfk_3 FOREIGN KEY (articel_id) REFERENCES group_project_article (articel_id) ON DELETE CASCADE
);

INSERT INTO group_project_comment (comment, made_by, comment_time, comment_to, articel_id) VALUES ('Interesting article', 'next', '2019-02-05 06:34:15', null, 15);
INSERT INTO group_project_comment (comment, made_by, comment_time, comment_to, articel_id) VALUES ('I agree, this guy is a genius', 'next', '2019-02-05 06:45:20', 1, 15);
INSERT INTO group_project_comment (comment, made_by, comment_time, comment_to, articel_id) VALUES ('Disagree', 'next', '2019-02-05 06:47:44', null, 15);
INSERT INTO group_project_comment (comment, made_by, comment_time, comment_to, articel_id) VALUES ('Why?', 'next', '2019-02-05 06:47:56', 3, 15);
INSERT INTO group_project_comment (comment, made_by, comment_time, comment_to, articel_id) VALUES ('Because it''s unoriginal', 'next', '2019-02-05 06:48:19', 4, 15);
INSERT INTO group_project_comment (comment, made_by, comment_time, comment_to, articel_id) VALUES ('Too true', 'next', '2019-02-05 06:47:56', 2, 15);
INSERT INTO group_project_comment (comment, made_by, comment_time, comment_to, articel_id) VALUES ('me too', 'next', '2019-02-05 06:47:56', 1, 15);
INSERT INTO group_project_comment (comment, made_by, comment_time, comment_to, articel_id) VALUES ('I''m just happy to be here', 'next', '2019-02-05 06:47:56', null, 15);
INSERT INTO group_project_comment (comment, made_by, comment_time, comment_to, articel_id) VALUES ('first', 'next', '2019-02-05 06:47:56', null, 5);